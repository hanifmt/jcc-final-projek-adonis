# JCC Final Projek Adonis

Untuk menjalankan Projek :
1. Clone terlebih dahulu ke local anda
```https://gitlab.com/hanifmt/jcc-final-projek-adonis.git```
2. Masuk ke folder projek ```cd jcc-final-projek-adonis```
3. Install npm terlebih dahulu ```npm install```
4. Buat file baru dengan nama ```.env```
5. Salin kode berikut ke file ```.env```
```
PORT=3333
HOST=0.0.0.0
NODE_ENV=development
APP_KEY=UxHgUjS5oJKz67e4j7cnBrARbHG1V84V
DRIVE_DISK=local
DB_CONNECTION=mysql
MYSQL_HOST=localhost
MYSQL_PORT=3306
MYSQL_USER=root
MYSQL_PASSWORD=
MYSQL_DB_NAME=main_bersama
HASH_DRIVER=bcrypt
```
6. Jalan kan projek melalui terminal ```node ace serve --watch```
7. Buka browser dan masukan alamat ```https://localhost:3333/docs```
<hr>

```
Nama    : Hanif Muhammad Tsabit
Email   : hanifmtsabit@gmail.com
Catatan :
Saya ucapkan terima kasih sebesar besarnya terhadap penyelanggara Jabar Coding Camp dan juga para Trainer yang sudah bersusah payah meluangkan waktunya untuk membimbing dan juga memberikan banyak ilmu baru kepada saya
Tetapi sebelumnya saya mohon maaf juga yang sebesar besarnya karena kelemahan saya dalam menangkap materi yang diberikan serta dalam pengerjaan tugas harian terutama tugas akhir ini tidak maksimal dan tidak memenuhi requirement yang di berikan dan diharapkan
Mungkin ini feedback dari saya yaitu berbaiki dan matangkan kembali sistem dalam pengajaran dan pemberian tugas terutama tugas akhir ini karena instruksi yang diberikan dan contoh yang diberikannya juga sedikit membingungkan serta mungkin tidak sesuai dalam pengistuksiannya
Serta dalam proses rekaman penyampaian materinya sudah sangat baik hanya saja sebaiknya menurut saya jangan live coding di jadikan materi karena mungkin banyak penyampainnya yang menjadi berbelit dan lama memakan waktu
Alangkah baiknya penyampaian materi kalau bisa menjadi lebih terarah dan langsung step by step supaya mudah dipahami dan di ikuti serta tidak bosen dalam memperhatikannya
Tapi sekali lagi saya ucapkan banyak terima kasih karena berkat Jabar Coding Camp berserta para Trainer karena sudah memberikan saya ilmu ilmu baru yang semoga kelak ilmu ini dapat saya manfaatkan untuk diri saya pribadi dan umumnya untuk membantu berkontribusi dalam pengembangan Jawa Barat
```
