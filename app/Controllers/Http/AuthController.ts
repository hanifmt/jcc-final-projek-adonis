import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import RegisterValidator from 'App/Validators/RegisterValidator'
import User from 'App/Models/User'
import LoginValidator from 'App/Validators/LoginValidator'

export default class AuthController {

  /**
   *
   * @swagger
   *
   * /register:
   *  post:
   *    tags:
   *      - Autentikasi
   *    produces:
   *      - application/json
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              name:
   *                type: string
   *                example: name
   *              email:
   *                type: string
   *                example: name@mail.com
   *              password:
   *                type: string
   *                example: password
   *              role:
   *                type: string
   *                example: user|owner
   *    responses:
   *     '200':
   *       description: successful operation
   *    security: []
   * /login:
   *  post:
   *    tags:
   *      - Autentikasi
   *    produces:
   *      - application/json
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              email:
   *                type: string
   *                example: name@mail.com
   *              password:
   *                type: string
   *                example: password
   *    responses:
   *     '200':
   *       description: successful operation
   *    security: []
   *
   */

  public async register ({request, response}: HttpContextContract) {
    try {
      await request.validate(RegisterValidator)

      await User.create({
        name: request.input('name'),
        email: request.input('email'),
        password: request.input('password'),
        role: request.input('role')
      })

      response.status(201).json({status: 'Success', message: 'Register Success'})
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async login ({auth, request, response}: HttpContextContract) {
    try {
      await request.validate(LoginValidator)
      const email = request.input('email')
      const password = request.input('password')

      const token = await auth.use('api').attempt(email, password)

      response.status(200).json({status: 'Success', data: token})
    } catch (error) {
      if(error.guard){
        response.badRequest(error.message)
      }else{
        response.badRequest(error.messages)
      }
    }
  }

}
