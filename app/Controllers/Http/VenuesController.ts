import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
import { DateTime } from 'luxon'

export default class VenuesController {

  /**
   *
   * @swagger
   *
   * /venues:
   *  get:
   *    tags:
   *      - Venues
   *    produces:
   *      - application/json
   *    responses:
   *     '200':
   *       description: successful operation
   *    security:
   *      - bearerAuth: []
   * /venues/store:
   *  post:
   *    tags:
   *      - Venues
   *    produces:
   *      - application/json
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              name:
   *                type: string
   *                example: Bumi Arena
   *              address:
   *                type: string
   *                example: Jl. Bumi Arena
   *              phone:
   *                type: string
   *                example: '022147'
   *    responses:
   *     '200':
   *       description: successful operation
   *    security:
   *      - bearerAuth: []
   * /venues/update/{id}:
   *  put:
   *    tags:
   *      - Venues
   *    produces:
   *      - application/json
   *    parameters:
   *      - in: path
   *        name: id
   *        required: true
   *        schema:
   *          type: integer
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              name:
   *                type: string
   *                example: Bumi Arena
   *              address:
   *                type: string
   *                example: Jl. Bumi Arena
   *              phone:
   *                type: string
   *                example: '022147'
   *    responses:
   *     '200':
   *       description: successful operation
   *    security:
   *      - bearerAuth: []
   * /venues/{id}:
   *  get:
   *    tags:
   *      - Venues - Bookings
   *    produces:
   *      - application/json
   *    parameters:
   *      - in: path
   *        name: id
   *        required: true
   *        schema:
   *          type: integer
   *    responses:
   *     '200':
   *       description: successful operation
   *    security:
   *      - bearerAuth: []
   * /venues/{id}/bookings:
   *  post:
   *    tags:
   *      - Venues - Bookings
   *    produces:
   *      - application/json
   *    parameters:
   *      - in: path
   *        name: id
   *        required: true
   *        schema:
   *          type: integer
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              play_date_start:
   *                type: string
   *                example: 2021-10-25 08:00:00
   *              play_date_end:
   *                type: string
   *                example: 2021-10-25 10:00:00
   *    responses:
   *     '200':
   *       description: successful operation
   *    security:
   *      - bearerAuth: []
   *
   */

  public async index ({auth, response}:HttpContextContract) {
    if(auth.user?.role == 'owner'){
      const data = await Database.from('venues')
                    .join('users', 'users.id', '=', 'venues.user_id')
                    .select('venues.id','venues.name', 'venues.address', 'venues.phone')
                    .select('users.name as owner')
      response.status(200).json({status: 'Success', data: data})
    }
    else {
      response.status(401).json({message: 'Only owner can access this page'})
    }
  }

  public async store ({auth, request, response}: HttpContextContract) {
    const userRole = auth.user?.role
    const userId = auth.user?.id

    try {
      if(userRole == 'owner'){
        await request.validate(CreateVenueValidator)

        await Database.table('venues').returning('id').insert({
          name: request.input('name'),
          address: request.input('address'),
          phone: request.input('phone'),
          user_id: userId
        })

        response.status(201).json({status: 'Success', message: 'Created by : ' + userRole})
      }else {
        response.status(401).json({message: 'Only owner can access this page'})
      }
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async update ({auth, params, request, response}: HttpContextContract) {
    const id = params.id
    const userRole = auth.user?.role

    try {
      if(userRole == 'owner'){
        await Database.from('venues').where('id', id).update({
          name: request.input('name'),
          address: request.input('address'),
          phone: request.input('phone')
        })

        response.status(200).json({status: 'Success', message: 'Updated by : ' + userRole})
      }else {
        response.status(401).json({message: 'Only owner can access this page'})
      }
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async show ({params, response}: HttpContextContract) {
    const id = params.id
    const dataVenues = await Database.from('venues').select('*').where('id', id).firstOrFail()
    const dataBookings = await Database.from('bookings').select('*').where('venue_id', id)

    interface bookingObj {
      play_date_start: DateTime,
      play_date_end: DateTime,
      venue_id: number,
      user_id: number
    }

    let data = {
      id: dataVenues.id,
      name: dataVenues.name,
      address: dataVenues.address,
      phone: dataVenues.phone,
      bookings: []
    }

    for (let item of dataBookings){
      let play_date_start = item.play_date_start
      play_date_start = play_date_start.toString()
      let play_date_end = item.play_date_end
      play_date_end = play_date_end ? play_date_end.toString() : null

      let dataBook: bookingObj = {
        play_date_start: play_date_start,
        play_date_end: play_date_end,
        venue_id: item.venue_id,
        user_id: item.user_id
      }

      data.bookings.push(dataBook)
    }

    response.status(200).json({status: 'success', data: data})
  }

  public async storeBooking ({params, auth, request, response}: HttpContextContract) {
    const id = params.id
    const userId = auth.user?.id
    const userName = auth.user?.name

    try {
      await request.validate(CreateBookingValidator)

      await Database.table('bookings').insert({
        play_date_start: request.input('play_date_start'),
        play_date_end: request.input('play_date_end'),
        venue_id: id,
        user_id: userId
      })

      response.status(201).json({status: 'Success', message: 'Created by : ' + userName})
    } catch (error) {
      response.badRequest(error.messages)
    }
  }
}
